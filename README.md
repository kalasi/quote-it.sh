# quote-it.sh

Adds a greater-than character ('>') to the beginning of each line of text in
your X-clipboard via `xsel`. For example:

```
this is
a string of
many lines of text
```

After running `quote-it`, it becomes:

```
>this is
>a string of
>many lines of text
```

### Installation

Run this after cloning the repo:

```
make install
```

This copies it to `/usr/local/bin/quote-it`.

### LICENSE

UNLICENSED. Do whatever you want with it. Details in `LICENSE.md`.
